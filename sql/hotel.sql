DROP SCHEMA IF EXISTS hotel;
CREATE SCHEMA hotel DEFAULT CHARACTER SET utf8;
USE hotel;

CREATE TABLE sobe (
	id INT AUTO_INCREMENT, 
	tipSobe VARCHAR(20) NOT NULL, 
    brojKreveta INT NOT NULL,
    cenaNocenja DOUBLE NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE rezervacije (
	id INT AUTO_INCREMENT, 
	soba INT NOT NULL, 
	datumUlaska DATETIME NOT NULL,
    datumIzlaska DATETIME NOT NULL,
    gost VARCHAR(30) NOT NULL,

	PRIMARY KEY(id), 
	  
	FOREIGN KEY (soba) REFERENCES sobe(id)
		ON DELETE RESTRICT
);
	
INSERT INTO sobe (id, tipSobe, brojKreveta, cenaNocenja) VALUES (1, 'Studio', 2, 2000.00);
INSERT INTO sobe (id, tipSobe, brojKreveta, cenaNocenja) VALUES (2, 'Suite', 1, 2500.00);
INSERT INTO sobe (id, tipSobe, brojKreveta, cenaNocenja) VALUES (3, 'Family room', 4, 3500.00);
INSERT INTO sobe (id, tipSobe, brojKreveta, cenaNocenja) VALUES (4, 'Interconected rooms', 2, 2500.00);
INSERT INTO sobe (id, tipSobe, brojKreveta, cenaNocenja) VALUES (5, 'Interconected rooms', 2, 2000.00);
INSERT INTO sobe (id, tipSobe, brojKreveta, cenaNocenja) VALUES (6, 'Suite', 2, 3000.00);

INSERT INTO rezervacije (id, soba, datumUlaska, datumIzlaska, gost) VALUES (1, 6, '2017-11-01 12:00:00', '2017-11-10 10:00:00', 'Petar Petrović');
INSERT INTO rezervacije (id, soba, datumUlaska, datumIzlaska, gost) VALUES (2, 3, '2017-11-05 13:00:00', '2017-11-10 08:00:00', 'Marko Marković');
INSERT INTO rezervacije (id, soba, datumUlaska, datumIzlaska, gost) VALUES (3, 6, '2017-11-19 03:00:00', '2017-11-22 03:00:00', 'Jovan Jovanović');
INSERT INTO rezervacije (id, soba, datumUlaska, datumIzlaska, gost) VALUES (4, 3, '2017-11-10 12:30:00', '2017-11-20 07:00:00', 'Petar Petrović');
INSERT INTO rezervacije (id, soba, datumUlaska, datumIzlaska, gost) VALUES (5, 4, '2017-11-10 12:00:00', '2017-12-02 08:00:00', 'Marko Marković');
INSERT INTO rezervacije (id, soba, datumUlaska, datumIzlaska, gost) VALUES (6, 5, '2017-11-10 12:00:00', '2017-12-02 08:00:00', 'Marko Marković');