package club.ivanstajic.hotel.model;

import java.util.ArrayList;

public class Soba {
	
	protected int id;
	protected String tipSobe;
	protected int brojKreveta;
	protected double cenaNocenja;
	protected ArrayList<Rezervacija> rezervacije = new ArrayList<Rezervacija>();
	
	public Soba(int id, String tipSobe, int brojKreveta, double cenaNocenja) {
		super();
		this.id = id;
		this.tipSobe = tipSobe;
		this.brojKreveta = brojKreveta;
		this.cenaNocenja = cenaNocenja;
	}

	@Override
	public String toString() {
		return "Soba [id=" + id + ", tipSobe=" + tipSobe + ", brojKreveta=" + brojKreveta + ", cenaNocenja="
				+ cenaNocenja + ", rezervacije=" + rezervacije + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Soba other = (Soba) obj;
		if (brojKreveta != other.brojKreveta)
			return false;
		if (Double.doubleToLongBits(cenaNocenja) != Double.doubleToLongBits(other.cenaNocenja))
			return false;
		if (id != other.id)
			return false;
		if (rezervacije == null) {
			if (other.rezervacije != null)
				return false;
		} else if (!rezervacije.equals(other.rezervacije))
			return false;
		if (tipSobe == null) {
			if (other.tipSobe != null)
				return false;
		} else if (!tipSobe.equals(other.tipSobe))
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipSobe() {
		return tipSobe;
	}

	public void setTipSobe(String tipSobe) {
		this.tipSobe = tipSobe;
	}

	public int getBrojKreveta() {
		return brojKreveta;
	}

	public void setBrojKreveta(int brojKreveta) {
		this.brojKreveta = brojKreveta;
	}

	public double getCenaNocenja() {
		return cenaNocenja;
	}

	public void setCenaNocenja(double cenaNocenja) {
		this.cenaNocenja = cenaNocenja;
	}

	public ArrayList<Rezervacija> getRezervacije() {
		return rezervacije;
	}

	public void setRezervacije(ArrayList<Rezervacija> rezervacije) {
		this.rezervacije = rezervacije;
	}
	

}
