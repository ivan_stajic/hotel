package club.ivanstajic.hotel.model;

import java.util.Date;

import club.ivanstajic.hotel.utils.PomocnaKlasa;

public class Rezervacija {
	
	protected int id;
	protected Soba soba;
	protected Date datumUlaska;
	protected Date datumIzlaska;
	protected String gost;
	
	public Rezervacija(int id, Soba soba, Date datumUlaska, Date datumIzlaska, String gost) {
		super();
		this.id = id;
		this.soba = soba;
		this.datumUlaska = datumUlaska;
		this.datumIzlaska = datumIzlaska;
		this.gost = gost;
	}

	@Override
	public String toString() {
		return "Rezervacija [id=" + id + ", datumUlaska=" + PomocnaKlasa.sdf.format(datumUlaska) + ", datumIzlaska="
				+ PomocnaKlasa.sdf.format(datumIzlaska) + ", gost=" + gost + ", soba=" + soba.getId() + ", ukupna vrednost rezervacije"
				+ ukupnaVrednostRezervacija() + "]";
	}
	
	public String toFormatedString() {
		return String.format("%-4s %-20%s %-20s %-20s %7d %27.2f", id, PomocnaKlasa.sdf.format(datumUlaska), PomocnaKlasa.sdf.format(datumIzlaska),
				gost, soba.getId(), ukupnaVrednostRezervacija());
	}
	
	public double ukupnaVrednostRezervacija() {
		long kraj = datumIzlaska.getTime();
		long pocetak = datumUlaska.getTime();
		long razlika = kraj - pocetak;
		int razlikaInt = (int)(razlika/(1000 * 60 * 60 * 24));
		
		return (double)(razlikaInt * soba.getCenaNocenja());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rezervacija other = (Rezervacija) obj;
		if (datumIzlaska == null) {
			if (other.datumIzlaska != null)
				return false;
		} else if (!datumIzlaska.equals(other.datumIzlaska))
			return false;
		if (datumUlaska == null) {
			if (other.datumUlaska != null)
				return false;
		} else if (!datumUlaska.equals(other.datumUlaska))
			return false;
		if (gost == null) {
			if (other.gost != null)
				return false;
		} else if (!gost.equals(other.gost))
			return false;
		if (id != other.id)
			return false;
		if (soba == null) {
			if (other.soba != null)
				return false;
		} else if (!soba.equals(other.soba))
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Soba getSoba() {
		return soba;
	}

	public void setSoba(Soba soba) {
		this.soba = soba;
	}

	public Date getDatumUlaska() {
		return datumUlaska;
	}

	public void setDatumUlaska(Date datumUlaska) {
		this.datumUlaska = datumUlaska;
	}

	public Date getDatumIzlaska() {
		return datumIzlaska;
	}

	public void setDatumIzlaska(Date datumIzlaska) {
		this.datumIzlaska = datumIzlaska;
	}

	public String getGost() {
		return gost;
	}

	public void setGost(String gost) {
		this.gost = gost;
	}
	

}
