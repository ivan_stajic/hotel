package club.ivanstajic.hotel.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import club.ivanstajic.hotel.model.Soba;

public class SobaDAO {

	public static List<Soba> getAll(Connection conn) {
		List<Soba> sobe = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, tipSobe, brojKreveta, cenaNocenja FROM sobe";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String tip = rset.getString(index++);
				int brKreveta = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				
				sobe.add(new Soba(id, tip, brKreveta, cena)); 
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sobe;
	}

	
}
