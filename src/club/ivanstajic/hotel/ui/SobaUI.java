package club.ivanstajic.hotel.ui;

import java.util.List;

import club.ivanstajic.hotel.dao.SobaDAO;
import club.ivanstajic.hotel.model.Soba;

public class SobaUI {
	
	public static void ispisiSveSobe() {
		List<Soba> sobe = SobaDAO.getAll(ApplicationUI.getConn());

		System.out.println();
		System.out.println("===============================================");
		System.out.printf("%-4s %-20s %10s %10s",  
				"ID", 
				"TIP",
				"BR.KREVETA",
				"CENA"); System.out.println();
		System.out.println("===============================================");
		for (Soba soba : sobe) {
			System.out.printf("%-4s %-20s %10s %10.2f",  
					soba.getId(),  
					soba.getTipSobe(),
					soba.getBrojKreveta(),
					soba.getCenaNocenja());
			System.out.println();
			System.out.println("-----------------------------------------------");
		}
		System.out.println();
	}


}
