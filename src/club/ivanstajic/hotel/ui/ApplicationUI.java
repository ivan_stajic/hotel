package club.ivanstajic.hotel.ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import club.ivanstajic.hotel.utils.PomocnaKlasa;

public class ApplicationUI {
	
private static Connection conn;
	
	static {
		// otvaranje konekcije, jednom na pocetku aplikacije
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");
			// otvaranje konekcije
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/hotel?useSSL=false", //TODO zameni naziv baze podataka
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			// kraj aplikacije
			System.exit(0);
		}
	}
	
	public static void main(String[] args)  {
		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				SobaUI.ispisiSveSobe();
				break;
//			case 2:
//				RezervacijaUI.ispisiSveRezervacijeZaPeriod();
//				break;
//			case 3:
//				RezervacijaUI.izmeniDatumZaRezevaciju();
//				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}

		// zatvaranje konekcije, jednom na kraju aplikacije
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Banka - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - prikaz svih soba");
		System.out.println("\tOpcija broj 2 - prikaz svih rezervacija za zadati vremenski period");
		System.out.println("\tOpcija broj 3 - izmena datuma i vremena za postojecu rezervaciju sobe");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

	public static Connection getConn() {
		return conn;
	}

}
